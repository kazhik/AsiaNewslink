+++
title = "中東以西"
sidemenu = "true"
+++
[アフガニスタン](#afghanistan) | [中央アジア](#centralasia) | [中東](#middleeast) | [バルカン半島](#balkans)

## アフガニスタン<a name="afghanistan"></a>

*   [Daily Outlook Afghanistan](http://outlookafghanistan.net/) （アフガニスタン／英語）

    2004年に創刊された英字紙。
    
## 中央アジア<a name="centralasia"></a>

*   [KABAR](http://eng.kabar.kg/) （キルギス／英語）

    キルギスの国営通信社。

*   [The Times of Central Asia](https://www.timesca.com/) （キルギス／英語）

    中央アジア五ヶ国のニュースをカバーする。

*   [Radio Free Europe / Radio Liberty](http://www.rferl.org/) （チェコ／英語）

    1989年の東欧革命以前はCIAの援助で旧ソ連・東欧圏のニュースを流していたラジオ局。現在はアメリカ議会から資金援助を受けている。中央アジアに関する貴重な情報源。

*   [スラブ研究リンク集](http://src-h.slav.hokudai.ac.jp/DB/link-top.html) （日本／日本語）

    北海道大学スラブ研究センターが作成するディレクトリ。旧ソ連・東欧地域のWebページはここから。

## 中東<a name="middleeast"></a>

*   [Aljazeera](http://www.aljazeera.com/) （カタール／英語）

    中東地域の最も有名なテレビ局。

*   [Al Bawaba](http://www.albawaba.com/) （ヨルダン、UAE、イギリス／英語）

    中東全域をカバーするニュース＆ポータルサイト。

*   [The Tehran Times](http://www.tehrantimes.com/) （イラン／英語）

    イランの二大英字紙の一つ。

*   [イランイスラム共和国放送](http://japanese.irib.ir/) （イラン／日本語）

    イランのラジオ放送。ニュースはテキストと音声で提供されている。

*   [The Jerusalem Post](http://www.jpost.com/) （イスラエル／英語）

    イスラエルの代表的な英字紙。

*   [Hurriyet](http://www.hurriyet.com.tr/english/home/) （トルコ／英語）

    トルコの二大紙のうちの一つ。

*   [Rudaw](http://www.rudaw.net/english)（イラク／英語）

    イラク北部で活動するクルディスタン民主党に近いメディアグループが運営する。


## バルカン半島<a name="balkans"></a>

*   [Beta News Agency](http://www.beta-press.com/default.asp?lan=en) （セルビア／英語）

    1992年に設立された独立系の通信社。

*   [B92](http://www.b92.net/eng/) （セルビア／英語）

    独立系のラジオ局。ユーゴ空爆の際には政府から弾圧を受けたがインターネット上で情報発信を続けた。

*   [Athens News Agency - Macedonian Press Agency](https://www.amna.gr/en) （ギリシャ／英語）

    Athens News Agency と Macedonian Press Agencyが2008年に合併してできた通信社。