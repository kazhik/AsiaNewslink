+++
title = "東北アジア"
sidemenu = "true"
+++
[中国](#china) | [香港](#hongkong) | [台湾](#taiwan) | [チベット](#tibet) | [ウイグル(東トルキスタン)](#uyghur) | [モンゴル](#mongol) | [韓国、朝鮮](#korea) 

## 中国<a name="china"></a>

*   [人民網](http://j.peopledaily.com.cn/home.html)（中国／日本語）

    中国共産党の機関紙『人民日報』のオンライン版。

*   [チャイナネット](http://www.china.org.cn/japanese/index.htm)（中国／日本語）

    中国政府によるニュースサイト。『北京週報』の記事も読める。

*   [新華社通信](http://jp.xinhuanet.com/)（中国／日本語）

    国営通信社。

*   [中国中央電視台](http://english.cctv.com/)（中国／英語）

    国営テレビ局。

*   [China Daily](http://www.chinadaily.com.cn/)（中国／英語）

    北京で発行されている英字紙。系列のBusiness WeeklyやShanghai Starの記事も読める。

*   [中国人権](http://www.hrichina.org/)（アメリカ／英語）

    中国の人権問題に関する情報を集めている。ニューヨークが本拠。

*   [サーチナ](http://www.searchina.ne.jp/)（日本／日本語）

    中国情報に関するポータル。[「日本の一企業として、日本の法令を遵守するのはもちろん、当社の中国に対する立場は、日本政府の立場と同一のものを堅持いたします」](http://info.searchina.ne.jp/media/?p=129)という方針で運営されている。

*   [日中労働情報フォーラム](http://www.chinalaborf.org/)（日本／日本語）

    中国の労働運動に関する情報を掲載。運営母体は総評の流れを汲む。

*   [朝鮮族ネット](http://www.searchnavi.com/~hp/chosenzoku/index.html)（中国／日本語）

    中国東北部に住む朝鮮族に関するニュースサイト。

## 香港<a name="hongkong"></a>

* [South China Morning Post](http://www.scmp.com/)（香港／英語）

    最大手の英字紙。

*   [The Standard](http://www.thestandard.com.hk/)（香港／英語）

    香港第二の英字紙。


## 台湾<a name="taiwan"></a>


*   [The Taipei Times（台北時報）](http://www.taipeitimes.com/)（台湾／英語）

    発行部数最大と言われる中国語紙『自由時報』の系列で、1999年6月に創刊された。[Taiwan News（英文台湾新聞）](http://www.etaiwannews.com/)、[The China Post（英文中国郵報）](http://www.chinapost.com.tw/)に次ぐ台湾で三番目の英字紙。

*   [フォーカス台湾](http://japan.cna.com.tw/)（台湾／日本語）

    独立系の中央通訊社によるニュースサイト。

*   [台湾週報](http://www.roc-taiwan.org/jp_ja/post/578.html)（台湾／日本語）

    中華民国台北駐日経済文化代表處（大使館にあたる機関）による在日台湾人向けの週刊紙。政治・経済から芸能まで、記事は非常に充実している。台湾の各新聞の記事が要約されているのも便利。

## チベット<a name="tibet"></a>

*   [TibetInfoNet](http://www.tibetinfonet.net/)（イギリス／英語）

    中立的な立場からチベットに関するニュースを提供する。

*   [I Love TIBET!](http://www.tibet.to/)（日本／日本語）

    チベットに関する総合サイト。日本で行われるチベット関係のイベントがアナウンスされる。[リンク集](http://www.tibet.to/link.htm)も充実。

## ウイグル（東トルキスタン）<a name="uyghur"></a>

*   [世界ウイグル会議](http://www.uyghurcongress.org/jp/)（ドイツ／日本語）

    東トルキスタンの独立をめざすウイグル人の国際組織。傘下に日本の組織として[日本ウイグル協会](http://uyghur-j.org/)がある。
    
## モンゴル<a name="mongol"></a>

*   [The UB Post](http://ubpost.mongolnews.mn/) （モンゴル／英語）

    ウランバートルで発行されている週刊紙。

## 韓国、朝鮮<a name="korea"></a>

*   [朝鮮日報](http://japan.chosun.com/) （韓国／日本語）

    日本の植民地だった時代から発行されている韓国で最大手の新聞。論調はやや保守的。

*   [中央日報](http://japanese.joins.com/) （韓国／日本語）

    サムスン（三星）系の新聞。比較的経済記事が多いが、北朝鮮関連のセクションもある。

*   [ハンギョレ新聞](http://japan.hani.co.kr/) （韓国／日本語）

    1980年代の民主化運動を背景として創刊された進歩派的なスタンスの新聞。
    
*   [KBS](http://world.kbs.co.kr/japanese/) （韓国／日本語）

    韓国の公共放送局。テキストだけでなくネットラジオのサービスもある。

*   [東洋経済日報](http://www.toyo-keizai.co.jp/) （日本／日本語）

    韓国経済や在日社会の動向を中心にした週刊の新聞。在日系としては[統一日報](http://www.onekoreanews.net/)に次ぐ。

*   [民団新聞](http://www.mindan.org/) （日本／日本語）

    在日本大韓民国民団の機関紙。在日韓国人社会の状況がわかる。

*   [朝鮮新報](http://chosonsinbo.com/jp/) （日本／日本語）

    「朝鮮民主主義人民共和国を朝鮮人民の真正な政権として支持し、共和国政府のすべての路線と政策の基礎であるチュチェ思想を自己の指導理念、すべての活動の指導的指針としている、共和国の海外公民団体」（[朝鮮総聯について](http://www.chongryon.com/japan/chongryon/c1-3/sou1-3-1.htm)）である[朝鮮総連](http://www.chongryon.com/index-j.htm)の機関紙。週3回刊。

*   [朝鮮中央通信](http://www.kcna.co.jp/index-e.htm) （日本／英語）

    朝鮮民主主義人民共和国の国営通信社。サイト自体は在日の朝鮮通信社によって運営されている。
