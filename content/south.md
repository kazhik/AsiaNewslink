+++
title = "南アジア"
sidemenu = "true"
+++
[インド](#india) | [バングラデシュ](#bangladesh) | [ブータン](#bhutan) | [ネパール](#nepal) | [スリランカ](#srilanka) | [モルディブ](#maldives) | [パキスタン](#pakistan)

## インド<a name="india"></a>

*   [Press Trust of India](http://www.ptinews.com/) （インド／英語）

    通信社。日本のメディアではPTI通信と呼ばれている。

*   [The Indian Express](http://www.indian-express.com/) （インド／英語）

    インド全域にネットワークを持つ。政治的にはリベラル。

*   [The Hindu](http://www.hinduonnet.com/) （インド／英語）

    マドラスで発行されている南インドの代表的新聞。

*   [Deccan Herald](http://www.deccanherald.com/) （インド／英語）

    バンガロールで発行され、カルナータカ州を中心に南インドの情報をカバー。

*   [The Hindustan Times](http://www.hindustantimes.com/) （インド／英語）

    ニューデリーを中心に北インドの情報をカバー。国民会議派系。

*   [The Time of India](http://www.timesofindia.com/) （インド／英語）

    ボンベイを中心に西部インドの情報をカバー。論調は保守的。

*   [The Statesman](http://www.thestatesman.com/) （インド／英語）

    西ベンガル州を中心に東部インドの情報をカバー。中立系。

*   [The Kashmir Times](http://www.kashmirtimes.com/) （インド／英語）

    ジャンム・カシミール地方で発行されている日刊紙。

*   [India Today](http://www.india-today.com/) （インド／英語）

    週刊誌。

* [Darjeeling Times](http://darjeelingtimes.com/) （インド／英語）

    ダージリン地方のニュースを伝える。


## バングラデシュ<a name="bangladesh"></a>
*   [Prothom Alo](https://en.prothomalo.com/) （バングラデシュ／英語）

    発行部数第二位のベンガル語新聞。

*   [The Daily Star](http://thedailystar.net/) （バングラデシュ／英語）

    1991年に創刊された最大手の英字紙。

## ブータン<a name="bhutan"></a>

*   [Kuensel](http://www.kuenselonline.com/) （ブータン／英語）

    もともとは官報で、1992年から自治組織による発行に変わった新聞。週一回刊。

*   [Bhutan Times](https://www.bhutantimes.com/) （アメリカ／英語）

    ブータンのニュースを集積。

## ネパール<a name="nepal"></a>

*   [The Kathmandu Post](http://kathmandupost.ekantipur.com/) （ネパール／英語）

    1993年にはじめて民間企業から創刊された英字紙。

*   [Nepal Home Page](http://www.info-nepal.com/) （ネパール／英語）

    ディレクトリ型検索エンジン。


## スリランカ<a name="srilanka"></a>

*   [Sri Lanka News Web](http://www.info.lk/slnews/) （スリランカ／英語）

    最新のニュースと内外の主要メディアへのリンク。

*   [Daily News](http://www.dailynews.lk/) （スリランカ／英語）

    政府系のLakeHouseが発行する日刊紙。

*   [The Island](http://www.island.lk/) （スリランカ／英語）

    独立系の日刊紙。

*   [TamilNet](http://www.tamilnet.com/) （スリランカ／英語）

    少数民族のタミール人に関するニュース。

## モルディヴ<a name="maldives"></a>

*   [Haveeru Daily](http://www.haveeru.com.mv/english/) （モルディヴ／英語）

    モルディヴの最大手紙。

## パキスタン<a name="pakistan"></a>

*   [The News International](http://thenews.jang.com.pk/) （パキスタン／英語）

    ウルドゥ語の有力紙[『Jang』](http://www.jang.com.pk/jang/)と同じ系列の英字紙。

*   [DAWN](http://dawn.com/) （パキスタン／英語）

    南部のカラチで発行されている。政府の見解を反映する英字紙。

*   [The Nation](http://www.nation.com.pk/) （パキスタン／英語）

    ウルドゥ語の有力紙『Nawa-i-Waqt』と同じ系列の英字紙。ラホールやイスラマバードを中心に流通している。