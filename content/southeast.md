+++
title = "東南アジア"
sidemenu = "true"
+++

[フィリピン](#philippines) | [ブルネイ](#brunei) | [ベトナム](#vietnam) | [ラオス](#laos) | [カンボジア](#cambodia) | [タイ](#thai) | [ミャンマー](#myanmar)  | [マレーシア](#malaysia) | [シンガポール](#singapore) | [インドネシア](#indonesia) | [東チモール](#easttimor)

## フィリピン<a name="philippines"></a>

*   [Philippine Daily Inquirer](http://www.inquirer.net/) （フィリピン／英語）

    フィリピン最大の日刊紙。

*   [Manila Bulletin](http://www.mb.com.ph/) （フィリピン／英語）

    1900年にできたフィリピンで一番古い新聞。親政府的なスタンスを取ることで弾圧を免れてきた。

*   [The Philippine Star](http://www.philstar.com/) （フィリピン／英語）

    Inquirerの創設者の一部が1986年に創刊。

*   [日刊まにら新聞](http://www.manila-shimbun.com/) （フィリピン／日本語）

    マニラで発行されている日本語紙。

## ブルネイ<a name="brunei"></a>

*   [Borneo Bulletin](http://www.brunet.bn/news/bb/) （ブルネイ／英語）

    ブルネイで最も古い日刊の英字紙。


## ベトナム<a name="vietnam"></a>

*   [Nhan Dan](http://www.nhandan.com.vn/english/) （ベトナム／英語）

    ベトナム共産党の機関紙。

*   [Viet Nam News](http://vietnamnews.vnagency.com.vn/) （ベトナム／英語）

    国営通信社が発行する英字紙。

*   [Vietnam Economic Times](http://www.vneconomy.com.vn/eng/) （ベトナム／英語）

    経済紙。

*   [HOTNAM!](http://www.hotnam.com/) （ベトナム／日本語）

    旅行者や現地駐在の日本人を対象にしたポータルサイト。ニュース、フォーラム、ディレクトリがある。

*   [VIETJO](http://viet-jo.com/) （日本／日本語）

    ベトナムのニュースを毎日更新。主要なサイトへのリンクもある。

## ラオス<a name="laos"></a>

*   [Vientiane Times](http://www.vientianetimes.org.la/) （ラオス／英語）

    政府系の英字紙。

*   [VientianeTimes.com](http://www.vientianetimes.com/Headlines.html) （アメリカ／英語）

    他サイトでのラオスのニュースへのリンクが張られている。政府系の同名の新聞とは無関係。

## カンボジア<a name="cambodia"></a>

*   [Phnom Penh Post](http://www.phnompenhpost.com/) （カンボジア／英語）

    [The Cambodia Daily](http://www.cambodiadaily.com/)とともに二大英字紙の一つ。2008年に日刊紙となった。

*   [The Cambodia News.net](http://www.thecambodianews.net/) （オーストラリア／英語）

    各ニュースサイトのカンボジア関連記事を集める。

    
## タイ<a name="thai"></a>

*   [Bangkok Post](http://www.bangkokpost.com/) （タイ／英語）

    1946年に創刊された信頼性の高い英字紙。

*   [The Nation](http://www.nationmultimedia.com/) （タイ／英語）

    Bangkok Postと並ぶ英字紙。

*   [バンコク週報](http://www.bangkokshuho.com/) （タイ／日本語）

    週刊で発行している紙媒体の記事の一部をWeb上で提供している。定期購読者になると全文が読める。

*   [リンク・タイランド](http://www.linkthailand.com/) （日本／日本語）

    タイに関するディレクトリと最新のニュース。


## ミャンマー<a name="myanmar"></a>

*   [The Myanmar Times](https://www.mmtimes.com/) （ビルマ／英語）

    2000年に創刊された英字紙。カンボジアのThe Phnom Penh PostやタイのThe Bangkok Postと姉妹関係にある。


## マレーシア<a name="malaysia"></a>

*   [malaysiakini.com](http://www.malaysiakini.com/) （マレーシア／英語）

    1999年11月に創刊され、告発型の特ダネを連発しているオンライン紙。

*   [The Star](http://thestar.com.my/) （マレーシア／英語）

    与党系のマレーシア華人協会(MCA)をオーナーとするタブロイド紙。

*   [BERNAMA](http://www.bernama.com/) （マレーシア／英語）

    国営通信社。

*   [日馬プレス Online](http://www.nichimapress.com/) （マレーシア／日本語）

    月二回発行されている『日馬プレス』のWeb版。ニュースはほぼ毎日更新されている。


## シンガポール<a name="singapore"></a>

*   [The Straits Times](http://www.straitstimes.com/) （シンガポール／英語）

    最大手の英字紙。

*   [Today](http://www.todayonline.com/) （シンガポール／英語）

    The Straits Timesに次ぐ部数のフリーペーパー。


## インドネシア<a name="indonesia"></a>

*   [The Jakarta Post](http://www.thejakartapost.com/) （インドネシア／英語）

    ジャカルタで発行されている英字紙。

*   [Antara](http://www.antaranews.com/en/) （インドネシア／英語）

    国営通信社。

*   [NINDJA](http://www.nindja.com/) （日本／日本語）

    インドネシア民主化支援ネットワーク(NINDJA)のページ。人権問題や民主化に関するニュースをtwitterで伝えている。

*   [じゃかるた新聞](http://www.jakartashimbun.com/)（インドネシア／日本語）

    ジャカルタで発行されている日本語の日刊紙。

## 東ティモール<a name="easttimor"></a>

* [The Dili Weekly](http://www.thediliweekly.com/en/)（東ティモール／英語）

    2007年に創刊された週刊紙。記事は現地のテトゥン語と英語で書かれている。


*   [East Timor News](http://www.timor.com/) （アメリカ／英語）

    [The World News Network](http://www.wnnetwork.com/)が運営するサイト。東ティモールに関する各紙の報道を集積している。

*   [East Timor Action Network/U.S.](http://www.etan.org/)（アメリカ／英語）

