+++
title = "管理人室"
sidemenu = "true"
+++

　このページはアジア関連のニュースサイトを厳選してリンクしています。日本語か英語で書かれていること、ニュース性のある情報が掲載されていること、頻繁に更新されていること、読みやすいデザインになっていること、などを選択の条件とし、マスコミだけでなく市民団体や個人のページも取り上げています。

　リンク先が引っ越している、説明が間違っている、重要なWebページが漏れている、などといった問題にお気づきの方は[小池和彦(kazhik@gmail.com)](mailto:kazhik@gmail.com)宛にメールをください。