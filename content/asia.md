+++
title = "アジア全般"
sidemenu = "true"
+++
[マスメディア(日本)](#japan) | [マスメディア(海外)](#world) | [左翼、市民派](#leftwing) | [総合情報、リンク集](#links)

## マスメディア(日本)<a name="japan"></a>

* [Yahoo! Japanニュース](https://news.yahoo.co.jp/hl?c=c_int) （日本／日本語）
  
    最新のトピックに関するニュースを集積し、関連サイトにリンクを張っている。

* [CNN.co.jp](https://www.cnn.co.jp/) （日本／日本語）

    CNNの日本語版。これで足りなければ<a href="http://edition.cnn.com/ASIA">CNN.com</a>へ。

* [読売新聞](http://www.yomiuri.co.jp/world/) （日本／日本語）

    最新の記事とそれ以外の記事のリストが出る。記事は数時間でリストから消え、本文のファイル自体も数日で消滅。

* [朝日新聞](http://www.asahi.com/international/) （日本／日本語）
 
    読売新聞と同じく速報性は高いが、記事は数日で消える。

* [日本経済新聞](https://www.nikkei.com/international/) （日本／日本語）

    経済に関するニュースはやはり日経が強い。

* [毎日ｊｐ](https://mainichi.jp/asia-oceania/) （日本／日本語）

    毎日新聞が運営。

* [世界日報](http://www.worldtimes.co.jp/category/world/) （日本／日本語）

    統一協会系の新聞。論調はきわめて反動的だが、記事は充実している。

* [アジア・マンスリー](https://www.jri.co.jp/report/medium/asia/) （日本／日本語）

    日本総合研究所による。「アジア11カ国の経済動向」の他、経済分析の論文を毎月数本掲載。


## マスメディア(海外)<a name="world"></a>

* [BBC News](http://www.bbc.com/news/world/asia) （イギリス／英語）

    さすが老舗、と思わせる充実した内容。記事に関連するWebページへはリンクが張られている。大きなニュースに関しては詳細なリンク集が作られることもある。

* [Yahoo! News](https://www.yahoo.com/news/world/) （アメリカ／英語）

    各ニュースサイトの記事をトピックごとに整理している。ホットなニュースを追いかけたい時に便利。
    

* [TUFS Media](http://www.el.tufs.ac.jp/tufsmedia/) （日本／日本語）

    東京外国語大学の学生らがニュースサイトの記事を翻訳して掲載している。


## 左翼、市民派<a name="leftwing"></a>

*   [OurPlanet-TV](http://www.ourplanet-tv.org) （日本／日本語）

    環境や人権をテーマにした番組を配信するネットメディア。

*   [レイバーネット日本](http://www.labornetjp.org/) （日本／日本語）

    世界各国の労働運動のニュース。レイバーネットの組織は韓国にもあるので、韓国の労働運動に関してはニュースが充実している。

*   [アムネスティ日本](http://www.amnesty.or.jp/news/) （日本／日本語）

    アムネスティ・インターナショナル日本支部のニュースリリース。

*   [Green Left Weekly](https://www.greenleft.org.au/) （オーストラリア／英語）

    オーストラリアの左翼誌。International Newsのところでアジアのニュースがよく取り上げられる。

*   [World Socialist Web Site](http://www.wsws.org/en/topics/continent/asia/) （オーストラリア／英語）

    第四インタナショナル国際委員会（トロツキスト系）のニュースサイト。

*   [Human Rights Watch](http://www.hrw.org/) （アメリカ／英語）

    人権団体ヒューマン・ライツ・ウォッチのニュース。

*   [OneWorld.net](http://www.oneworld.net/) （イギリス／英語）

    300を超える人権・環境団体が発信するニュースを集約。国別、テーマ別にニュースを検索することもできる。

## 総合情報、リンク集<a name="links"></a>

* [Lists of newspapers - Wikipedia](https://en.wikipedia.org/wiki/Lists_of_newspapers#Asia)（アメリカ／英語）

    国ごとの新聞のリスト。


*   [NativeWeb Resources: Asia](http://www.nativeweb.org/resources/nations_web_sites_information/asia/) （アメリカ／英語）

    世界各地の先住民に関するWebページのディレクトリ。
