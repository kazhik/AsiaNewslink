---
author: "Koike Kazuhiko"
date: 1999-01-13
title: asian wave
weight: 10
---

　[asian wave](http://www.digitalnavi.co.jp/asia/default.htm)に「アジア・ニュースリンク」へのリンクがあるので驚いた。数日前にいくつかの検索エンジンに登録されたので、新着情報を見たのだろう。　asian waveとアジア・ニュースリンクは重なる部分が多い。あちらはデパート、こちらは専門店、というところか。私は以前からasian waveをブックマークに入れている。