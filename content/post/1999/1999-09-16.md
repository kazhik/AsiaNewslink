---
author: "Koike Kazuhiko"
date: 1999-09-16
title: 東ティモール問題についてのWebニュース
weight: 10
---

　JCA-NETで[東ティモール問題についてのWebニュース](http://www.jca.apc.org/web-news/reg-easttimor/index.shtml)が始まった。Web上で読むだけでなく、メールで送ってもらうこともできる。同じコーナーにある[リンク集](http://www.jca.apc.org/jca-net/jpd/east-timor.html)も充実している。