---
author: "Koike Kazuhiko"
date: 1999-09-06
title: ASIANOW
weight: 10
---

　CNNのサイトに[ASIANOW](http://japan.cnn.com/ASIANOW/)というコーナーができた。TIMEとASIAWEEKの記事も読めるようになっている。TIMEは今までリンクしていなかったし、ASIAWEEKもほとんど読んでいなかったので、この二つがCNNに統合されたのは非常に便利になった。