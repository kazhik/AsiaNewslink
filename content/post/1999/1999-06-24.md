---
author: "Koike Kazuhiko"
date: 1999-06-24
title: 社会主義ニュースサイトの成功の秘訣
weight: 10
---

　Wired Newsに[社会主義ニュースサイトの成功の秘訣](http://www.hotwired.co.jp/news/news/2642.html)という記事が出ている。[World Socialist Web Site](http://www.wsws.org/)について、ボランティアの記者たちの水平的なネットワークでつくられていることに注目したものだ。今までこのサイトの本拠地はアメリカだと思っていたが、オーストラリアだったようだ。さっそくリンク集の記述を修正。