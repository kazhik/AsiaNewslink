---
author: "Koike Kazuhiko"
date: 1998-12-30
title: India Bashing
weight: 10
---

　[News and Newspapers - Indian Subcontinent & The International Media](http://www.geocities.com/TimesSquare/Labyrinth/7504/newspaper.html)に"India Bashing"という項目があるのを発見。リンクされている[ASIA REFLECT](http://www.asia-reflect.net/)を見てみると、[Weekly News Roundup](http://www.asia-reflect.net/news/index.htm)にアフガニスタン、インド、イラン、カシミール、パキスタンのニュースが掲載され、[Issues and Views](http://www.asia-reflect.net/issues/)ではカシミール地方の帰属問題でインドを批判する論説が掲載されている。要注目か。