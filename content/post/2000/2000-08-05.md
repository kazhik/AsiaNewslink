---
author: "Koike Kazuhiko"
date: 2000-08-05
title: 中国人権民主化運動情報センター
weight: 10
---

　中国人権民主化運動情報センターのWebページに[英文ページ](http://www.89-64.com/english/indexen.html)と[和文ページ](http://www.89-64.com/japanese/index.html)ができているのを発見。和文ページは残念ながらイントロダクションだけのようだ。