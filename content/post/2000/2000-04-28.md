---
author: "Koike Kazuhiko"
date: 2000-04-28
title: ネットが崩すマレーシアの言論統制
weight: 10
---

　asahi.comに[ネットが崩すマレーシアの言論統制](http://www.asahi.com/tech/feature/20000421genron.html)という記事が出ている。[malaysiakini.com](http://www.malaysiakini.com/)というオンライン紙の紹介だ。昨年11月に創刊され、告発型の特ダネを連発して人気を集めているという。さっそくマレーシアの項にリンクを追加。